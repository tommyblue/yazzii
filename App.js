import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import Match from "./components/match";

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {matchStatus: null, finalResult: 0, };
        this.startMatch = this.startMatch.bind(this);
        this.endMatch = this.endMatch.bind(this);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Text>YAZZII</Text>
                </View>
                <View style={{flex: 1}}>
                    {this.pageContent()}
                </View>
            </View>
        );
    }

    pageContent() {
        if (this.state.matchStatus === 'running') {
            return (<Match onEndMatch={this.endMatch}/>);
        }
        return (
            <View style={{flex: 1}}>
                {this.showResults()}
                <Button onPress={this.startMatch} title="New Match" />
            </View>
        );
    }

    showResults() {
        if (this.state.matchStatus === 'finshed') {
            return (<Text>Final result: {this.state.finalResult}</Text>);
        }
    }

    startMatch() {
        this.setState({matchStatus: 'running', finalResult: 0});
    }

    endMatch(finalResult) {
        this.setState({matchStatus: 'finished', finalResult});
    }
}

const styles = StyleSheet.create({
    logo: {
        height: 40,
        marginTop: 20
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10
    },
});
