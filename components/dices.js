import React from "react";
import {Button, View} from "react-native";

import Dice from "./dice";

export default class extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                {this.showDices()}
            </View>
        );
    }

    showDices() {
        const dices = [];
        for (i = 0; i < 5; i++) {
            dices.push(
                <Dice
                    key={`dice_${i}`}
                    diceIndex={i}
                    value={this.props.lastRoll[i]}
                    onToggleDice={this.props.onToggleDice}
                    isSelected={this.props.selectedDices[i] === true}
                />
            );
        }
        return (
            <View style={{flex:1, flexDirection: 'row'}}>{dices}</View>
        );
    }
}
