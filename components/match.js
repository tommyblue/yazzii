import React from "react";
import { StyleSheet, Text, View, Button } from 'react-native';
import _ from "lodash";

import Dices from "./dices";
import Table from "./table";

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rolls: 0,
            lastRoll: [],
            selectedDices: [false, false, false, false, false],
            missingForBonus: -62,
            results: {
                ones: null,
                twos: null,
                threes: null,
                fours: null,
                fives: null,
                sixes: null,
                trips: null,
                quads: null,
                full: null,
                small: null,
                large: null,
                yazzii: null,
                chance: null,
            }
        };

        this.rollDices = this.rollDices.bind(this);
        this.setResult = this.setResult.bind(this);
        this.toggleDice = this.toggleDice.bind(this);
    }

    render() {
        const disabledButton = (
            this.state.rolls >= 3 ||
            _.every(this.state.selectedDices, v => v === true)
        );
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <Table
                        results={this.state.results}
                        lastRoll={this.state.lastRoll}
                        onSetResult={this.setResult}
                        missingForBonus={this.state.missingForBonus}
                    />
                </View>
                <View style={{ height: 100 }}>
                    <Dices
                        lastRoll={this.state.lastRoll}
                        selectedDices={this.state.selectedDices}
                        onToggleDice={this.toggleDice}
                    />
                    <Button
                        onPress={this.rollDices}
                        title={`Rolls (${this.state.rolls}/3)`}
                        disabled={disabledButton}
                    />
                </View>
            </View>
        );
    }

    toggleDice(index) {
        const selectedDices = [...this.state.selectedDices];
        selectedDices[index] = !selectedDices[index];
        this.setState({ ...this.state, selectedDices })
    }

    setResult(key, value) {
        const results = { ...this.state.results };
        results[key] = value;

        let missingForBonus = this.getMissingForBonus(results);

        this.setState({
            ...this.state,
            results,
            missingForBonus,
            rolls: 0,
            lastRoll: [],
            selectedDices: [false, false, false, false, false, false],
        });

        if (_.filter(results, r => r === null).length === 0) {
            const bonus = missingForBonus >= 0 ? 35 : 0;
            this.props.onEndMatch(_.sum(results) + bonus);
        }
    }

    getMissingForBonus(results) {
        let val = -62;
        const keys = ['ones', 'twos', 'threes', 'fours', 'fives', 'sixes'];
        _.forEach(keys, (k) => {
            if (results[k]) {
                val = val + results[k];
            }
        });
        return val;
    }

    rollDices() {
        if (this.state.rolls > 2) {
            return
        }
        const values = [];
        for (i = 0; i < 5; i++) {
            if (this.state.selectedDices[i]) {
                values.push(this.state.lastRoll[i]);
            } else {
                values.push(Math.floor(Math.random() * 6) + 1);
            }
        }
        this.setState({ ...this.state, lastRoll: values, rolls: this.state.rolls + 1 })
    }
}
