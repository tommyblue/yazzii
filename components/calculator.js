import _ from "lodash";

export const calculateSum = (v, val) => (_.sum(_.filter(v, (x) => x === val)));
export const calculateChance = (v) => _.sum(v);
export const calculateTrips = (v) => calculateMulti(v, 3);
export const calculateQuads = (v) => calculateMulti(v, 4);

const calculateMulti = (v, limit) => {
    let matchLimit = false;
    const results = [0, 0, 0, 0, 0, 0];

    _.forEach(v, x => {
        results[x-1]++;
        if (results[x-1] >= limit) {
            matchLimit = true;
        }
    });

    return matchLimit ? _.sum(v) : 0;
}

export const calculateYazzii = (v) => {
    let isYazzii = true;
    _.forEach(v, x => {
        if (x !== v[0]) {
            isYazzii = false;
        }
    });

    return isYazzii ? 50 : 0;
}

export const calculateSmall = (v) => {
    const results = [0, 0, 0, 0, 0, 0];
    _.forEach(v, x => {
        results[x-1]++;
    });
    if (results[2] === 0 || results[3] === 0) {
        return 0;
    }
    let isSmall = false;
    let notZeros = 0;
    _.forEach(results, r => {
        if (r === 0) {
            notZeros = 0;
        } else {
            notZeros++;
        }
        if (notZeros === 4) {
            isSmall = true;
            return;
        }
    });
    return isSmall ? 30 : 0;
}

export const calculateLarge = (v) => {
    let isLarge = true;
    const results = [0, 0, 0, 0, 0, 0];
    _.forEach(v, x => {
        results[x-1]++;
        if (results[x-1] > 1) {
            isLarge = false;
        }
    });

    if (!isLarge) {
        return 0;
    }

    if (results[1] === 0 || results[2] === 0 || results[3] === 0 || results[4] === 0) {
        return 0;
    }

    return 40;
}

export const calculateFull = (v) => {
    const results = [0, 0, 0, 0, 0, 0];
    _.forEach(v, x => {
        results[x-1]++;
    });
    return _.filter(results, k => k !== 0).length === 2 ? 25 : 0;
}
