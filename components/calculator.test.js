import React from 'react';
import _ from 'lodash';

import {
    calculateChance,
    calculateFull,
    calculateLarge,
    calculateQuads,
    calculateSmall,
    calculateSum,
    calculateTrips,
    calculateYazzii,
} from './calculator';

it('calculates trips', () => {
    const manifest = [
        [[2, 3, 1, 2, 5], 0],
        [[5, 3, 1, 1, 2], 0],
        [[5, 3, 5, 1, 5], 19],
        [[1, 1, 1, 1, 1], 5],
        [[3, 4, 4, 3, 4], 18],
        [[6, 5, 5, 1, 5], 22],
    ];

    _.forEach(manifest, el => {
        expect(calculateTrips(el[0])).toBe(el[1]);
    });
});

it('calculates small', () => {
    const manifest = [
        [[5, 2, 1, 5, 6], 0],
        [[2, 2, 4, 5, 2], 0],
        [[1, 2, 4, 6, 5], 0],
        [[5, 6, 6, 1, 2], 0],
        [[3, 2, 4, 3, 3], 0],
        [[3, 3, 3, 4, 3], 0],
        [[5, 1, 2, 1, 3], 0],
        [[1, 2, 2, 3, 4], 30],
        [[4, 2, 1, 4, 3], 30],
        [[5, 3, 4, 6, 1], 30],
        [[1, 2, 4, 3, 5], 30],
        [[4, 3, 3, 1, 2], 30],
    ];

    _.forEach(manifest, el => {
        expect(calculateSmall(el[0])).toBe(el[1]);
    });
});

it('calculates large', () => {
    const manifest = [
        [[1, 1, 3, 5, 2], 0],
        [[6, 6, 6, 4, 1], 0],
        [[4, 1, 6, 4, 6], 0],
        [[1, 6, 6, 1, 1], 0],
        [[2, 2, 3, 2, 3], 0],
        [[5, 1 ,1 ,1 ,5], 0],
        [[1, 1, 6, 6, 6], 0],
        [[2, 4, 4, 4, 2], 0],
        [[3, 6, 3, 6, 3], 0],
        [[3, 5, 2, 6, 1], 0],
        [[1, 2, 3, 4, 5], 40],
        [[2, 3, 4, 5, 6], 40],
        [[3, 2, 5, 1, 4], 40],
        [[6, 5, 4, 3, 2], 40],
        [[5, 4, 3, 2, 1], 40],
    ];

    _.forEach(manifest, el => {
        expect(calculateLarge(el[0])).toBe(el[1]);
    });
});

it('calculates full', () => {
    const manifest = [
        [[1, 1, 3, 5, 2], 0],
        [[6, 6, 6, 4, 1], 0],
        [[4, 1, 6, 4, 6], 0],
        [[1, 6, 6, 1, 1], 25],
        [[2, 2, 3, 2, 3], 25],
        [[5, 1 ,1 ,1 ,5], 25],
        [[1, 1, 6, 6, 6], 25],
        [[2, 4, 4, 4, 2], 25],
        [[3, 6, 3, 6, 3], 25],
    ];

    _.forEach(manifest, el => {
        expect(calculateFull(el[0])).toBe(el[1]);
    });
});

it('calculates chance', () => {
    const manifest = [
        [[1, 1, 3, 5, 2], 12],
        [[6, 6, 6, 4, 1], 23],
        [[4, 1, 6, 4, 6], 21],
    ];

    _.forEach(manifest, el => {
        expect(calculateChance(el[0])).toBe(el[1]);
    });
});
