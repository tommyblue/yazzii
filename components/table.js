import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import _ from "lodash";

import {
    calculateChance,
    calculateFull,
    calculateLarge,
    calculateQuads,
    calculateSmall,
    calculateSum,
    calculateTrips,
    calculateYazzii,
} from "./calculator";

export default class extends React.Component {
    render() {
        let total = _.sum(_.map(this.props.results, (k) => k ? k : 0));
        if (this.props.missingForBonus >= 0) {
            total += 35;
        }
        const tableHead = [`Results (Tot: ${total})`];
        const tableData = [
            [
                this.getValue("ones", calculateSum, 1),
                this.getValue("trips", calculateTrips)

            ],
            [
                this.getValue("twos", calculateSum, 2),
                this.getValue("quads", calculateQuads)
            ],
            [
                this.getValue("threes", calculateSum, 3),
                this.getValue("full", calculateFull)
            ],
            [
                this.getValue("fours", calculateSum, 4),
                this.getValue("small", calculateSmall)
            ],
            [
                this.getValue("fives", calculateSum, 5),
                this.getValue("large", calculateLarge)
            ],
            [
                this.getValue("sixes", calculateSum, 6),
                this.getValue("yazzii", calculateYazzii)

            ],
            [
                this.getBonus(),
                this.getValue("chance", calculateChance)
            ],
        ];

        return (
            <View style={{ flex: 1 }}>
                <Table>
                    <Row data={tableHead} style={styles.head} textStyle={styles.headText}/>
                    <Rows data={tableData} style={styles.row} textStyle={styles.text}/>
                </Table>
            </View>
        );
    }

    getBonus() {
        if (this.props.missingForBonus >= 0) {
            return (<Text style={{fontWeight: 'bold'}}>Bonus: 35</Text>);
        }
        return (<Text>Bonus (if > 62): {this.props.missingForBonus}</Text>);
    }

    getValue(key, checkFn, val) {
        const value = this.props.results[key];
        const label = _.capitalize(key);
        if (!_.isNull(value)) {
            return (
                <Text style={{fontWeight: 'bold'}}>{label}: {value}</Text>
            );
        }

        if(_.isEmpty(this.props.lastRoll)) {
            return (<Text>{label}:</Text>);
        }

        const temp = checkFn(this.props.lastRoll, val);

        return (
            <TouchableOpacity onPress={this.props.onSetResult.bind(this, key, temp)}>
                <View style={{flex: 1}}>
                    <Text>
                        {label}: ({temp})
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    head: { height: 40, backgroundColor: '#f1f8ff'},
    headText: {textAlign: 'center', fontWeight: 'bold'},
    text: { marginLeft: 5 },
    row: { height: 30 }
});
