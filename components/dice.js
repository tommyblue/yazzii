import React from "react";
import {Image, StyleSheet, TouchableHighlight} from "react-native";

const dice_1 = require("../assets/images/dice_1.png");
const dice_2 = require("../assets/images/dice_2.png");
const dice_3 = require("../assets/images/dice_3.png");
const dice_4 = require("../assets/images/dice_4.png");
const dice_5 = require("../assets/images/dice_5.png");
const dice_6 = require("../assets/images/dice_6.png");

const dices = {
    dice_1,
    dice_2,
    dice_3,
    dice_4,
    dice_5,
    dice_6,
};

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.toggleDice = this.toggleDice.bind(this);
    }

    render() {
        return (
            <TouchableHighlight
                style={[styles.container, this.props.isSelected && styles.selected]}
                onPress={this.toggleDice}
            >
                <Image
                    style={styles.dice}
                    source={dices[`dice_${this.props.value}`]}
                    resizeMode="contain"
                />
            </TouchableHighlight>
        );
    }

    toggleDice() {
        this.props.onToggleDice(this.props.diceIndex);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    dice: {
        flex:1,
        height: undefined,
        width: undefined,
        margin: 2
    },
    selected: {
        borderWidth: 0.5,
        borderColor: 'red'
    }
});
